# First Google Kubernetes Engine

module "cluster_gke" {
    source                      = "./../../modules/gke"
    project                     =   var.project
    cluster_name                = "${var.gke_cluster_name}"
    location                    = "${var.region}"
    initial_node_count          = "${var.gke_initial_node}"
    machine_type                = "${var.gke_machine_type}"
    node_pools_scopes           = "${var.gke_node_pools_scopes}"
    min_node_count              = "${var.gke_min_node_count}"
    max_node_count              = "${var.gke_max_node_count}"
    remove_default_node_pool    = "${var.gke_remove_default_node_pool}" 
    master_ipv4_cidr_block      = "${var.master_ipv4_cidr_block}"
    preemptible                  = "${var.gke_preemptible}"
}
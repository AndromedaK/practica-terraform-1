# Variables Globales
variable "project" {
  default = "hidden-terrain-279900"
}

# hola holsdfshkd

variable "region" {
  default = "us-central1"
}

/*variable "zona" {
  default = "us-central1-c"
}
*/
# Google Kubernetes Engine
variable "gke_cluster_name" {
  default = "oreji-cluster"
}

variable "gke_initial_node" {
  default = "2"
}

variable "gke_machine_type"{
  default = "n1-standard-2"
}

variable "gke_node_pools_scopes" {

  default = ["https://www.googleapis.com/auth/compute","https://www.googleapis.com/auth/pubsub"]
}


variable "gke_min_node_count" {
  default = "1"
}

variable "gke_max_node_count" {
  default = "2"
}

variable "gke_remove_default_node_pool" {
  default = "true"
}
variable "master_ipv4_cidr_block" {
  default = "10.100.80.0/28"
}

variable "gke_preemptible" {
  default = "false"
}